package de.gsohs.bme5;

import io.javalin.Javalin;
import io.javalin.rendering.template.JavalinThymeleaf;

import java.util.Map;

public class MyServer {
    public static void main(String[] args) {
        Javalin javalin = Javalin.create(config -> {
            config.fileRenderer(new JavalinThymeleaf());
        }).start(8080);
        javalin.get("/hello", ctx -> {
            String name = "World";
            if(ctx.queryParamMap().containsKey("name")) {
               name=  ctx.queryParam("name");
            }
            ctx.result("Hello "+name+"!");
        });
        javalin.get("/myinfo", ctx -> {
            StringBuilder out = new StringBuilder();
            ctx.headerMap().forEach((k,v) -> {
                out.append(k+" : "+v+"\n");
            });
            ctx.result(out.toString());
        });
        javalin.post("/formpost", ctx -> {
            StringBuilder out = new StringBuilder();
            ctx.formParamMap().forEach((k,v) -> {
                out.append(k+" : "+v+"\n");
            });
            ctx.result(out.toString());
        });
        javalin.get(
                "/thymeleaf",
                ctx -> ctx.render(
                        "firsthtml.html",
                        Map.of("fruits",new String[] {"apples","bananas","peaches"})
                )
        );
    }
}
